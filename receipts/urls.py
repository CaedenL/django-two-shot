from django.urls import path
from receipts.views import (
    show_receipts,
    new_receipt,
    show_categories,
    show_accounts,
    new_category,
    new_account,
)


urlpatterns = [
    path("receipts/accounts/create/", new_account, name="create_account"),
    path("receipts/categories/create/", new_category, name="create_category"),
    path("receipts/accounts/", show_accounts, name="account_list"),
    path("receipts/categories/", show_categories, name="category_list"),
    path("receipts/create/", new_receipt, name="create_receipt"),
    path("receipts/", show_receipts, name="home"),
]
