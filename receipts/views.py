from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import NewReceiptForm, NewCategoryForm, NewAccountForm


@login_required
def show_receipts(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt": receipt}
    return render(request, "receipts/details.html", context)


@login_required
def new_receipt(request):
    if request.method == "POST":
        form = NewReceiptForm(request.POST, user=request.user)
        if form.is_valid():
            form = form.save(False)
            form.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = NewReceiptForm(user=request.user)

    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def show_categories(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "receipts/categories.html", context)


@login_required
def show_accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipts/accounts.html", context)


@login_required
def new_category(request):
    if request.method == "POST":
        form = NewCategoryForm(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = NewCategoryForm()

    context = {"form": form}

    return render(request, "receipts/create_category.html", context)


@login_required
def new_account(request):
    if request.method == "POST":
        form = NewAccountForm(request.POST)
        if form.is_valid():
            form = form.save(False)
            form.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = NewAccountForm()

    context = {"form": form}

    return render(request, "receipts/create_account.html", context)
